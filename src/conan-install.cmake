cmake_minimum_required(VERSION 3.11.0)

macro(conan_install)
    find_file(CONANFILE 
        NAMES conanfile.py conanfile.txt
        PATHS ${CMAKE_SOURCE_DIR}
        NO_DEFAULT_PATH
        NO_CMAKE_FIND_ROOT_PATH
    )
    if(NOT CONAN_EXPORTED AND NOT "${CONANFILE}" STREQUAL "CONANFILE-NOTFOUND") 
        message(STATUS "Conanfile found: ${CONANFILE}")
        configure_file(${CONANFILE} ${CMAKE_BINARY_DIR} COPYONLY NO_SOURCE_PERMISSIONS )

        if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
            message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
            file(DOWNLOAD "https://raw.githubusercontent.com/conan-io/cmake-conan/v0.16.1/conan.cmake"
                        "${CMAKE_BINARY_DIR}/conan.cmake"
                        TLS_VERIFY ON)
        endif()

        include(${CMAKE_BINARY_DIR}/conan.cmake)

        conan_cmake_autodetect(settings)
        conan_cmake_install(PATH_OR_REFERENCE ${CONANFILE}
                    INSTALL_FOLDER ${CMAKE_BINARY_DIR}
                    BUILD missing
                    UPDATE
                    SETTINGS ${settings}
                    PROFILE ${CMAKE_CONAN_PROFILE_FILE})
    else()
        message(STATUS "No conanfile found, processing without conanfile")
    endif()
endmacro()
