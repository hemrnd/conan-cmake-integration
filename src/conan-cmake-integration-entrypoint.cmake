cmake_minimum_required(VERSION 3.15.0)

include(${CMAKE_CURRENT_LIST_DIR}/conan-profile.cmake)
handle_conan_profile()
set(CMAKE_PROJECT_INCLUDE ${CMAKE_CURRENT_LIST_DIR}/conan-install-caller.cmake CACHE STRING "Do conan install after project initialisation")