cmake_minimum_required(VERSION 3.11.0)

if(NOT CONAN_EXPORTED) 

    find_program(CONAN_CMD conan REQUIRED)

    function(find_conan_profile_variable)
        cmake_parse_arguments(ARGUMENTS "" "OUTPUT;VARIABLE;PROFILE_FILE" "" ${ARGN})

        execute_process(COMMAND ${CONAN_CMD} profile get ${ARGUMENTS_VARIABLE} ${ARGUMENTS_PROFILE_FILE}
            RESULT_VARIABLE return_code
            OUTPUT_VARIABLE output
            ERROR_VARIABLE error
            OUTPUT_STRIP_TRAILING_WHITESPACE
            ERROR_STRIP_TRAILING_WHITESPACE)
        if ("${return_code}" STREQUAL "0")
            set(${ARGUMENTS_OUTPUT} ${output} PARENT_SCOPE)
        else()
            if("${error}" STREQUAL "ERROR: Key not found: '${ARGUMENTS_VARIABLE}'")
                message(STATUS "Variable ${ARGUMENTS_VARIABLE} not found in profile ${ARGUMENTS_PROFILE_FILE}")
                set(${ARGUMENTS_OUTPUT} ${ARGUMENTS_VARIABLE}-NOTFOUND PARENT_SCOPE)
            else()
                message(FATAL_ERROR "Can't parse profile ${ARGUMENTS_PROFILE_FILE}!\n${error}")
            endif()
        endif()
    endfunction()

    macro(set_conan_profile_environement)
        cmake_parse_arguments(ARGUMENTS "" "PROFILE_FILE" "" ${ARGN})

        execute_process(COMMAND ${CONAN_CMD} profile show ${ARGUMENTS_PROFILE_FILE}
            RESULT_VARIABLE return_code
            OUTPUT_VARIABLE output
            ERROR_VARIABLE error
            OUTPUT_STRIP_TRAILING_WHITESPACE
            ERROR_STRIP_TRAILING_WHITESPACE)
        if (NOT "${return_code}" STREQUAL "0")
            message(FATAL_ERROR "Can't parse profile ${ARGUMENTS_PROFILE_FILE}!\n${error}")
        endif()

        string(LENGTH ${output} LEN)
        string(FIND ${output} "[env]" ENV_POS)
        string(SUBSTRING ${output} ${ENV_POS} ${LEN} ENV_VARS)
        string(REPLACE "[env]\n"  "" ENV_VARS ${ENV_VARS})
        string(REPLACE "\n"  ";" ENV_VARS ${ENV_VARS})
        foreach(VAR ${ENV_VARS})
            string(STRIP ${VAR} ${VAR})
            string(FIND ${VAR} "=" EQUAL_POS)
            string(LENGTH ${VAR} LEN)
            string(SUBSTRING ${VAR} 0 ${EQUAL_POS} VAR_NAME)
            MATH(EXPR EQUAL_POS "${EQUAL_POS}+1")
            string(SUBSTRING ${VAR} ${EQUAL_POS} ${LEN} VAR_VALUE)
            string(REGEX MATCH ^CONAN_CMAKE_[A-Za-z0-9_]+$ MATCH_NAME ${VAR_NAME})
            if(MATCH_NAME)
                string(REPLACE "CONAN_" "" MATCH_NAME ${MATCH_NAME})
                set(${MATCH_NAME} ${VAR_VALUE})
            else()
                set(ENV{${VAR_NAME}} ${VAR_VALUE})
            endif()
        endforeach()
    endmacro()

    macro(set_cmake_toolchain_file_from_profile)
        cmake_parse_arguments(ARGUMENTS "" "PROFILE_FILE" "" ${ARGN})

        find_conan_profile_variable(
            VARIABLE env.CONAN_CMAKE_TOOLCHAIN_FILE
            OUTPUT CONAN_CMAKE_TOOLCHAIN_FILE
            PROFILE_FILE ${ARGUMENTS_PROFILE_FILE}
        )

        if(NOT DEFINED CMAKE_TOOLCHAIN_FILE)
            if (NOT "${CONAN_CMAKE_TOOLCHAIN_FILE}" STREQUAL "env.CONAN_CMAKE_TOOLCHAIN_FILE-NOTFOUND")
                set(CMAKE_TOOLCHAIN_FILE ${CONAN_CMAKE_TOOLCHAIN_FILE}) 
                message("Setting toolchain file to ${CONAN_CMAKE_TOOLCHAIN_FILE}")
            else()
                message("Toolchain file not found in profile, compiler wiil be detected by cmake.")
            endif()
        else()
            if(NOT "${CONAN_CMAKE_TOOLCHAIN_FILE}" STREQUAL "env.CONAN_CMAKE_TOOLCHAIN_FILE-NOTFOUND" 
                AND NOT "${CONAN_CMAKE_TOOLCHAIN_FILE}" STREQUAL "${CMAKE_TOOLCHAIN_FILE}")
                message(WARNING "Toolchain in profile ${CONAN_CMAKE_TOOLCHAIN_FILE} is different than manually set or cached: ${CMAKE_TOOLCHAIN_FILE}. It may cause problems!")
            endif()
        endif()
    endmacro()

    macro(handle_conan_build_type)
        cmake_parse_arguments(ARGUMENTS "" "PROFILE_FILE" "" ${ARGN})
        find_conan_profile_variable(
            OUTPUT CONAN_BUILD_TYPE
            VARIABLE settings.build_type
            PROFILE_FILE ${ARGUMENTS_PROFILE_FILE}
        )
        if (NOT "${CONAN_BUILD_TYPE}" STREQUAL "settings.build_type-NOTFOUND")
            if(NOT DEFINED CMAKE_BUILD_TYPE)
                set(CMAKE_BUILD_TYPE ${CONAN_BUILD_TYPE} CACHE STRING "Build type got from conan profile - don't edit!")
            elseif(NOT "${CMAKE_BUILD_TYPE}" STREQUAL "${CONAN_BUILD_TYPE}")
                if ("${CONAN_BUILD_TYPE}" STREQUAL "None")
                    message("Conan profile without build type, using CMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}!")
                else()
                    message(FATAL_ERROR "Conan profile build_type(=${CONAN_BUILD_TYPE}) and CMAKE_BUILD_TYPE(=${CMAKE_BUILD_TYPE}) mismatch.")
                endif()
            endif()
        endif()
    endmacro()
    

    macro(handle_conan_profile)
        if (DEFINED CMAKE_CONAN_PROFILE)
            find_file(
                CMAKE_CONAN_PROFILE_FILE ${CMAKE_CONAN_PROFILE}
                PATHS ${CMAKE_SOURCE_DIR}
                CMAKE_FIND_ROOT_PATH_BOTH 
            )
            if(${CMAKE_CONAN_PROFILE_FILE} STREQUAL CMAKE_CONAN_PROFILE_FILE-NOTFOUND)
                message("Can't find profile, it might be system one. Trying with ${CMAKE_CONAN_PROFILE}.")
                set(CMAKE_CONAN_PROFILE_FILE  ${CMAKE_CONAN_PROFILE})
            else()
                message("Found profile at " ${CMAKE_CONAN_PROFILE_FILE})
            endif()

            set_conan_profile_environement(
                PROFILE_FILE ${CMAKE_CONAN_PROFILE_FILE}
            )

            handle_conan_build_type(
                PROFILE_FILE ${CMAKE_CONAN_PROFILE_FILE}
            )
            

        endif()
    endmacro()
endif()
